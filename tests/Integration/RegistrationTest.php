<?php

namespace App\Tests\Integration;

use App\Service\Users\Registration;
use App\Tests\Mocks\MockMailer;
use Exception;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\InvalidSignatureException;

class RegistrationTest extends IntegrationTest
{

    public function testRegistration()
    {

        $mailer = $this->getContainer()->get(MailerInterface::class);
        $this->assertInstanceOf(MockMailer::class, $mailer);
        /** @var Registration $registration */
        $registration = $this->getContainer()->get(Registration::class);

        // Create a new user
        $user = $registration->initializeUser();
        $user->setEmail('tester@test.com');
        $user->setMotherTong($this->repositoryManager->language()->find(1));
        $user->addLearntLanguage($this->repositoryManager->language()->find(2));
        $registration->register($user, 'tarTEMpion123');

        // Verify he received the confirmation email, with valid verification url
        $message = $mailer->getLastMessage();
        $this->assertInstanceOf(TemplatedEmail::class, $message);
        $mailContext = $message->getContext();
        $url         = $mailContext['signedUrl'];
        $uriParams   = [];
        parse_str(parse_url($url, PHP_URL_QUERY), $uriParams);
        $this->assertEquals($this->repositoryManager->user()->findByEmail('tester@test.com')->getId(), $uriParams['id']);
        $this->assertArrayHasKey('expires', $uriParams);
        $this->assertArrayHasKey('id', $uriParams);
        $this->assertArrayHasKey('signature', $uriParams);
        $this->assertArrayHasKey('token', $uriParams);

        // Confirm the new user
        $registration->handleEmailConfirmation($url, $uriParams['id']);

        // Assert that new user is correctly saved on database
        $user = $this->repositoryManager->user()->findByEmail('tester@test.com');
        $this->assertTrue($user->isVerified());
        $this->assertEquals(1, $user->getMotherTong()->getId());
        $this->assertCount(1, $user->getLearntLanguages());
        $this->assertEquals(2, $user->getLearntLanguages()[0]->getId());
    }

    public function testInvalidRegistrations()
    {

        $mailer = $this->getContainer()->get(MailerInterface::class);
        $this->assertInstanceOf(MockMailer::class, $mailer);
        $mailer::$exception = new Exception('impossible to send email');
        $registration       = $this->getContainer()->get(Registration::class);

        // Create a new user, with failure
        $user = $registration->initializeUser();
        $user->setEmail('tester@test2.com');
        $user->setMotherTong($this->repositoryManager->language()->find(1));
        $user->addLearntLanguage($this->repositoryManager->language()->find(2));
        $this->assertNull($registration->register($user, 'tarTEMpion123'));

        // Assert that new user is not saved on database
        $user = $this->repositoryManager->user()->findByEmail('tester@test2.com');
        $this->assertNull($user);
    }

    public function testInvalidConfirmationLink()
    {

        $registration = $this->getContainer()->get(Registration::class);

        //Without id
        $url = 'http://localhost/en/verify/email?expires=1644473971&signature=cyZK2JOEE3O1gCrEWh2gNiRwyaRk75GO%2FtiQlmQrpg4%3D&token=Cz7b5M9hqSoAUa5jjnMC%2Fsxengkqu%2BS8JmoaViKnt24%3D';
        try {
            $registration->handleEmailConfirmation($url, null);
            $this->fail('invalid signature must throw an exception');
        } catch (InvalidSignatureException) {
            $this->assertTrue(true);
        }

        //With id but no user
        $url = 'http://localhost/en/verify/email?expires=1644473971&id=0&signature=cyZK2JOEE3O1gCrEWh2gNiRwyaRk75GO%2FtiQlmQrpg4%3D&token=Cz7b5M9hqSoAUa5jjnMC%2Fsxengkqu%2BS8JmoaViKnt24%3D';
        try {
            $registration->handleEmailConfirmation($url, 0);
            $this->fail('invalid signature must throw an exception');
        } catch (InvalidSignatureException) {
            $this->assertTrue(true);
        }

        //With invalid token
        $url = 'http://localhost/en/verify/email?expires=1644473971&id=2&signature=cyZK2JOEE3O1gCrEWh2gNiRwyaRk75GO%2FtiQlmQrpg4%3D&token=Cz7b5M9hqSoAUa5jjnMC%2Fsxengkqu%2BS8JmoaViKnt24%3D';
        try {
            $registration->handleEmailConfirmation($url, 2);
            $this->fail('invalid signature must throw an exception');
        } catch (InvalidSignatureException) {
            $this->assertTrue(true);
        }
    }
}