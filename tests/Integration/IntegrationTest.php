<?php

namespace App\Tests\Integration;

use App\Service\Orm\RepositoryManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

abstract class IntegrationTest extends KernelTestCase
{
    protected EntityManagerInterface $entityManager;
    protected RepositoryManager      $repositoryManager;

    protected function setUp(): void
    {
        parent::setUp();
        static::bootKernel();
        $this->entityManager     = $this->getContainer()->get(EntityManagerInterface::class);
        $this->repositoryManager = $this->getContainer()->get('orm');
        $this->entityManager->beginTransaction();
    }

    protected function tearDown(): void
    {
        $this->entityManager->rollback();
        parent::tearDown();
    }
}