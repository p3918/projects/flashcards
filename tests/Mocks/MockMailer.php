<?php

namespace App\Tests\Mocks;

use Exception;
use Symfony\Component\Mailer\Envelope;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\RawMessage;

class MockMailer implements MailerInterface
{
    public static ?Exception $exception = null;
    /** @var RawMessage[] */
    public array $messages = [];

    public function send(RawMessage $message, Envelope $envelope = null): void
    {
        if (self::$exception) {
            $exception = self::$exception;
            self::$exception = null;
            throw $exception;
        }
        $this->messages[] = $message;
    }

    public function getLastMessage(): bool|RawMessage
    {
        return end($this->messages);
    }
}