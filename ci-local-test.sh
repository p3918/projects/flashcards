#!/usr/bin/env sh

. .ci-local-env

./ci-build-test-image.sh
./ci-build-prod-image.sh
./ci-run-test.sh

docker compose -f docker-compose.yml up -d flashcards-prod-back

echo "RESULT : http://flashcards.${HOST}/"