<?php

namespace App\Controller;

use App\Form\RegistrationFormType;
use App\Service\Users\Registration;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;

class RegistrationController extends AbstractController
{

    #[Route('/{_locale}/register', name: 'register')]
    public function register(Request $request, TranslatorInterface $translator, Registration $registrationService): Response
    {
        $user = $registrationService->initializeUser();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$registrationService->register($user, $form->get('plainPassword')->getData())) {
                $form->addError(new FormError($translator->trans('pages.register.error')));
                return $this->render('registration/register.html.twig', [
                    'registrationForm' => $form->createView(),
                ]);
            }
            return $this->redirectToRoute('registered', ['_locale' => $request->getLocale()]);
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    #[Route('/{_locale}/registered', name: 'registered')]
    public function afterRegister(): Response
    {
        return $this->render('registration/after_register.html.twig', []);
    }

    #[Route('/{_locale}/verify/email', name: 'verify_email')]
    public function verifyUserEmail(Request $request, Registration $registrationService): Response
    {
        try {
            $registrationService->handleEmailConfirmation($request->getUri(), $request->get('id'));
        } catch (VerifyEmailExceptionInterface $exception) {
            $this->addFlash('error', $exception->getReason());
            return $this->redirectToRoute('register');
        }

        $this->addFlash('success', 'Your email address has been verified.');

        return $this->redirectToRoute('login');
    }
}
