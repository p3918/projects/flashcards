#!/usr/bin/env bash

# Start database, sleep hoping it will have enough time to start ... berk
docker compose -f docker-compose.yml -f docker-compose.local.yml up -d flashcards-bdd-tests flashcards-bdd
sleep 5

# Drop and recreate the dev database
sh console.sh doctrine:database:drop --force
sh console.sh doctrine:database:create
sh console.sh doctrine:schema:update --force
sh console.sh doctrine:fixtures:load -n

# Drop and recreate the tests database
sh console-test.sh doctrine:database:drop --force
sh console-test.sh doctrine:database:create
sh console-test.sh doctrine:schema:update --force
sh console-test.sh doctrine:fixtures:load -n -vvv