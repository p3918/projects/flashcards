#!/usr/bin/env sh

docker build --target php-test -t celinederoland/dev-cli-flashcards:${CI_COMMIT_SHORT_SHA} .
docker tag celinederoland/dev-cli-flashcards:${CI_COMMIT_SHORT_SHA} celinederoland/dev-cli-flashcards:latest