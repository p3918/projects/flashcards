#!/usr/bin/env sh

docker push celinederoland/dev-cli-flashcards:${CI_COMMIT_SHORT_SHA}
docker push celinederoland/dev-cli-flashcards:latest